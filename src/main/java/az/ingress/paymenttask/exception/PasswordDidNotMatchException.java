package az.ingress.paymenttask.exception;

import static az.ingress.paymenttask.exception.PaymentErrorCodes.PASSWORD_DID_NOT_MATCH;

public class PasswordDidNotMatchException extends PaymentGenericException {
    public PasswordDidNotMatchException(Object... arguments) {
        super(PASSWORD_DID_NOT_MATCH.code, PASSWORD_DID_NOT_MATCH.code, 403, arguments);
    }
}
