package az.ingress.paymenttask.exception;

import static az.ingress.paymenttask.exception.PaymentErrorCodes.INVALID_CREDENTIALS;

public class InvalidCredentialsException extends PaymentGenericException {

    public InvalidCredentialsException(Object... arguments) {
        super(INVALID_CREDENTIALS.code, INVALID_CREDENTIALS.code, 400, arguments);
    }
}
