package az.ingress.paymenttask.exception;

import static org.springframework.http.HttpHeaders.ACCEPT_LANGUAGE;

import az.ingress.paymenttask.service.impl.TranslationRepoServiceImpl;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

@RestControllerAdvice
@RequiredArgsConstructor
@Slf4j
public class GenericExceptionHandler {

    private final TranslationRepoServiceImpl translationRepoService;

    @ExceptionHandler(PaymentGenericException.class)
    public ResponseEntity<ErrorResponseDTO> handleGenericException(PaymentGenericException ex, WebRequest webRequest){
        ex.printStackTrace();
        var path = ((ServletWebRequest)webRequest).getRequest().getRequestURL().toString();
        String lang = webRequest.getHeader(ACCEPT_LANGUAGE);
        if(lang == null){
            lang = "en";
        }
        return createErrorResponse(ex,path,lang);
    }

    private ResponseEntity<ErrorResponseDTO> createErrorResponse(PaymentGenericException ex, String path, String lang) {
        ErrorResponseDTO build = ErrorResponseDTO.builder()
                .status(ex.getStatus())
                .code(ex.getCode())
                .path(path)
                .timestamp(OffsetDateTime.now())
                .message(translationRepoService.findByKey(ex.getMessage(), lang))
                .detail(translationRepoService.findByKey(ex.getMessage().concat("_DETAIL"), lang,ex.getArguments()))
                .build();
        return ResponseEntity.status(ex.getStatus()).body(build);
    }
}
