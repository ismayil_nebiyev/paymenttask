package az.ingress.paymenttask.exception;

import static az.ingress.paymenttask.exception.PaymentErrorCodes.PAYMENT_NOT_FOUND;

public class PaymentNotFoundException extends PaymentGenericException {
    public PaymentNotFoundException(Object... arguments) {
        super(PAYMENT_NOT_FOUND.code, PAYMENT_NOT_FOUND.code, 404, arguments);
    }
}
