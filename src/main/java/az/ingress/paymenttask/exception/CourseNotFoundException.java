package az.ingress.paymenttask.exception;

import static az.ingress.paymenttask.exception.PaymentErrorCodes.COURSE_NOT_FOUND;

public class CourseNotFoundException extends PaymentGenericException {
    public CourseNotFoundException(Object... arguments) {
        super(COURSE_NOT_FOUND.code, COURSE_NOT_FOUND.code, 404, arguments);
    }
}