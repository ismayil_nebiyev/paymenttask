package az.ingress.paymenttask.exception;

import static az.ingress.paymenttask.exception.PaymentErrorCodes.STUDENT_NOT_FOUND;

public class StudentNotFoundException extends PaymentGenericException {
    public StudentNotFoundException(Object... arguments) {
        super(STUDENT_NOT_FOUND.code, STUDENT_NOT_FOUND.code, 404, arguments);
    }
}