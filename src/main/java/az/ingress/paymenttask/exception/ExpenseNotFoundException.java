package az.ingress.paymenttask.exception;

import static az.ingress.paymenttask.exception.PaymentErrorCodes.EXPENSE_NOT_FOUND;

public class ExpenseNotFoundException extends PaymentGenericException {
    public ExpenseNotFoundException(Object... arguments) {
        super(EXPENSE_NOT_FOUND.code, EXPENSE_NOT_FOUND.code, 404, arguments);
    }
}
