package az.ingress.paymenttask.exception;

import static az.ingress.paymenttask.exception.PaymentErrorCodes.USER_ALREADY_EXIST;


public class UserAlreadyExistException extends PaymentGenericException {
    public UserAlreadyExistException(Object... arguments) {
        super(USER_ALREADY_EXIST.code, USER_ALREADY_EXIST.code, 409, arguments);
    }
}
