package az.ingress.paymenttask.exception;

import static az.ingress.paymenttask.exception.PaymentErrorCodes.USER_NOT_FOUND;

public class UsernameNotFoundException extends PaymentGenericException {
    public UsernameNotFoundException(Object... arguments) {
        super(USER_NOT_FOUND.code, USER_NOT_FOUND.code, 404, arguments);
    }
}
