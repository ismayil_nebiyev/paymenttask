package az.ingress.paymenttask.exception;

import static az.ingress.paymenttask.exception.PaymentErrorCodes.AUTHORITY_NOT_FOUND;
import static az.ingress.paymenttask.exception.PaymentErrorCodes.COURSE_NOT_FOUND;

public class AuthorityNotFoundException extends PaymentGenericException {
    public AuthorityNotFoundException(Object... arguments) {
        super(AUTHORITY_NOT_FOUND.code, AUTHORITY_NOT_FOUND.code, 404, arguments);
    }
}
