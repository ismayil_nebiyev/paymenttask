package az.ingress.paymenttask.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class PaymentGenericException extends RuntimeException{

    private final int status;
    private final String code;
    private final String message;
    private final transient Object[] arguments;

    public PaymentGenericException(String code, String message, int status, Object... arguments) {
        super(message);
        this.status = status;
        this.code = code;
        this.message = message;
        this.arguments = arguments == null ? new Object[0] : arguments;
    }

    public PaymentGenericException(String errorBody, HttpStatus statusCode){
        super(errorBody);
        this.status = statusCode.value();
        this.code = errorBody;
        this.message = errorBody;
        this.arguments = null;
    }


    @Override
    public String toString() {
        return String.format("%s{status=%d, code='%s', message='%s'}",this.getClass().getSimpleName(),status,code,
                message);
    }
}
