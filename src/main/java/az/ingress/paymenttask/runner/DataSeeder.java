package az.ingress.paymenttask.runner;

import az.ingress.paymenttask.domain.Authority;
import az.ingress.paymenttask.domain.User;
import az.ingress.paymenttask.domain.enumeration.Role;
import az.ingress.paymenttask.repository.AuthorityRepository;
import az.ingress.paymenttask.repository.UserRepository;
import java.util.HashSet;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class DataSeeder implements ApplicationRunner {

    private final AuthorityRepository authorityRepository;
    private final UserRepository userRepository;
    private final PasswordEncoder encoder;

    @Override
    public void run(ApplicationArguments args) throws Exception {

        if (authorityRepository.count() < 1){
            Authority authority1 = Authority.builder().role(Role.ADMIN).build();
            Authority authority2 = Authority.builder().role(Role.MANAGER).build();
            authorityRepository.saveAll(List.of(authority1,authority2));
        }

        if (userRepository.count() < 1){
            User user = User.builder()
                    .username("admin")
                    .password(encoder.encode("password"))
                    .authorities(new HashSet<>(authorityRepository.findAll()))
                    .build();
            userRepository.save(user);
            log.info("Admin saved {}",user);
        }
    }
}
