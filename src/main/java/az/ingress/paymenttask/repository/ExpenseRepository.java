package az.ingress.paymenttask.repository;

import az.ingress.paymenttask.domain.Expense;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExpenseRepository extends JpaRepository<Expense,Long> {
}
