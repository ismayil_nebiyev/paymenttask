package az.ingress.paymenttask.repository;

import az.ingress.paymenttask.domain.Student;
import com.api.first.development.openapi.model.StudentDto;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student,Long> {

    Optional<Student> findStudentByPhone(String phone);
}
