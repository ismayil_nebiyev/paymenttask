package az.ingress.paymenttask.repository;

import az.ingress.paymenttask.domain.User;
import java.util.Optional;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    @EntityGraph(attributePaths = {"authorities"}, type = EntityGraph.EntityGraphType.LOAD)
    Optional<User> findByUsername(String username);
}
