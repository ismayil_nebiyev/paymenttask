package az.ingress.paymenttask.repository;

import az.ingress.paymenttask.domain.Payment;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentRepository extends JpaRepository<Payment,Long> {

    @EntityGraph(attributePaths = {"course","student","course.members"}, type = EntityGraph.EntityGraphType.LOAD)
    Optional<Payment> findByStudent_Phone(String phone);
    @EntityGraph(attributePaths = {"course","student","course.members"}, type = EntityGraph.EntityGraphType.LOAD)
    List<Payment> findAll();
    @EntityGraph(attributePaths = {"course","student","course.members"}, type = EntityGraph.EntityGraphType.LOAD)
    Optional<Payment> findById(Long id);
}
