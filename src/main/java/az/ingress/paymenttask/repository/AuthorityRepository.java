package az.ingress.paymenttask.repository;


import az.ingress.paymenttask.domain.Authority;
import az.ingress.paymenttask.domain.enumeration.Role;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorityRepository extends JpaRepository<Authority,Long> {

    Optional<Authority> findByRole(Role authority);
}
