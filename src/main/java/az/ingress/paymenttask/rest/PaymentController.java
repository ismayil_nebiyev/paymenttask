package az.ingress.paymenttask.rest;

import az.ingress.paymenttask.service.PaymentService;
import com.api.first.development.openapi.api.PaymentApi;
import com.api.first.development.openapi.model.PaymentDto;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class PaymentController implements PaymentApi {

    private final PaymentService paymentService;

    @Override
    public ResponseEntity<List<PaymentDto>> getPaymentsList() {
        return ResponseEntity.ok(paymentService.findAll());
    }

    @Override
    public ResponseEntity<PaymentDto> savePayment(PaymentDto paymentDto) {
        return ResponseEntity.ok(paymentService.save(paymentDto));
    }

    @Override
    public ResponseEntity<PaymentDto> searchPayment(String phone) {
        return ResponseEntity.ok(paymentService.search(phone));
    }
}
