package az.ingress.paymenttask.rest;

import az.ingress.paymenttask.service.StudentService;
import com.api.first.development.openapi.api.StudentApi;
import com.api.first.development.openapi.model.StudentDto;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class StudentController implements StudentApi {

    private final StudentService studentService;

    @Override
    public ResponseEntity<Void> deleteStudent(Long id) {
        studentService.delete(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @Override
    public ResponseEntity<StudentDto> getStudent(Long id) {
        return ResponseEntity.ok(studentService.findById(id));
    }

    @Override
    public ResponseEntity<List<StudentDto>> getStudentsList() {
        return ResponseEntity.ok(studentService.findAll());
    }

    @Override
    public ResponseEntity<Void> saveStudent(StudentDto studentDto) {
        studentService.save(studentDto);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @Override
    public ResponseEntity<StudentDto> searchStudent(String phone) {
        return ResponseEntity.ok(studentService.findByPhone(phone));
    }


}
