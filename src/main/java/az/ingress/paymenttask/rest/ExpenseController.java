package az.ingress.paymenttask.rest;

import az.ingress.paymenttask.service.ExpenseService;
import com.api.first.development.openapi.api.ExpenseApi;
import com.api.first.development.openapi.model.ExpenseDto;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class ExpenseController implements ExpenseApi {

    private final ExpenseService expenseService;
    @Override
    public ResponseEntity<List<ExpenseDto>> getExpensesList() {
        return ResponseEntity.ok(expenseService.findAll());
    }

    @Override
    public ResponseEntity<ExpenseDto> saveExpense(ExpenseDto expenseDto) {
        return ResponseEntity.ok(expenseService.save(expenseDto));
    }
}
