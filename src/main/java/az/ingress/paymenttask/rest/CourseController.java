package az.ingress.paymenttask.rest;

import az.ingress.paymenttask.service.CourseService;
import com.api.first.development.openapi.api.CourseApi;
import com.api.first.development.openapi.model.CourseDto;
import com.api.first.development.openapi.model.JoinRequestDto;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class CourseController implements CourseApi {
    private final CourseService service;
    @Override
    public ResponseEntity<CourseDto> addStudent(JoinRequestDto joinRequestDto) {
        return ResponseEntity.ok(service.addStudent(joinRequestDto));
    }

    @Override
    public ResponseEntity<List<CourseDto>> getCoursesList() {
        return ResponseEntity.ok(service.getCourseList());
    }

    @Override
    public ResponseEntity<CourseDto> saveCourse(CourseDto courseDto) {
        return ResponseEntity.ok(service.save(courseDto));
    }

    @Override
    public ResponseEntity<CourseDto> searchCourse(String name) {
        return ResponseEntity.ok(service.getCourse(name));
    }
}
