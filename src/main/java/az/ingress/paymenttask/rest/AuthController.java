package az.ingress.paymenttask.rest;

import az.ingress.paymenttask.service.UserService;
import com.api.first.development.openapi.api.AuthApi;
import com.api.first.development.openapi.model.LoginResponseDto;
import com.api.first.development.openapi.model.RegisterRequestDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class AuthController implements AuthApi {

    private final UserService userService;

    @Override
    public ResponseEntity<LoginResponseDto> login(RegisterRequestDto registerRequestDto) {
        return ResponseEntity.accepted().body(userService.login(registerRequestDto));
    }

    @Override
    public ResponseEntity<Void> register(RegisterRequestDto registerRequestDto) {
        userService.register(registerRequestDto);
        return ResponseEntity.ok().build();
    }
}
