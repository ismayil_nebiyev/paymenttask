package az.ingress.paymenttask.security;

import az.ingress.paymenttask.domain.Authority;
import az.ingress.paymenttask.domain.User;
import az.ingress.paymenttask.exception.InvalidCredentialsException;
import az.ingress.paymenttask.repository.UserRepository;
import jakarta.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BasicAuthRequestFilter implements AuthService{
    private static final String AUTHORIZATION = "Authorization";
    private static final String BASIC = "Basic";
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public Optional<Authentication> getAuthentication(HttpServletRequest request) {

        var authorization = request.getHeader(AUTHORIZATION);
        String basic;
        if(authorization != null && authorization.startsWith(BASIC)){
            basic = authorization.substring(BASIC.length());
            byte[] decode = Base64.decodeBase64(basic);
            String[] credentials = new String(decode).split(":");
            if(credentials.length != 2){
                throw new InvalidCredentialsException();
            }
            User user = userRepository.findByUsername(credentials[0])
                    .orElseThrow(() -> new UsernameNotFoundException(credentials[0]));
            boolean matches = passwordEncoder.matches(credentials[1], user.getPassword());
            if(matches){
                Authentication authenticationBasic = getAuthenticationBasic(user);
                return Optional.of(authenticationBasic);
            }
        }
        return Optional.empty();
    }

    public Authentication getAuthenticationBasic(User user){
        List<?> roles = user.getAuthorities().stream().map(Authority::getRole).toList();
        List<GrantedAuthority> authorities = roles
                .stream()
                .map(a -> new SimpleGrantedAuthority(a.toString()))
                .collect(Collectors.toList());
        return new UsernamePasswordAuthenticationToken(user,"",authorities);
    }
}