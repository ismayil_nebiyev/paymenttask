package az.ingress.paymenttask.security;

import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.security.config.Customizer.withDefaults;

import az.ingress.paymenttask.domain.enumeration.Role;
import jakarta.servlet.DispatcherType;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.annotation.web.configurers.HeadersConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@RequiredArgsConstructor
public class SecurityConfig {

    private final JwtAuthFilterConfigurerAdapter filterConfigurerAdapter;
    private final AuthenticationEntryPointConfigurer authenticationEntryPointConfigurer;

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.csrf(AbstractHttpConfigurer::disable);
        http.sessionManagement((session) -> session
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS));

        http
                .authorizeHttpRequests((authz) -> authz
                        .requestMatchers( "/auth/v1/register").hasAuthority("ADMIN")
                        .requestMatchers( "/auth/v1/login").permitAll()
                        .anyRequest().authenticated()
                );

        http.formLogin((formLogin) -> {
            try {
                formLogin.disable().headers((headers) -> headers.frameOptions(
                        HeadersConfigurer.FrameOptionsConfig::disable));
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
        http.exceptionHandling((exceptionHandling) -> exceptionHandling
                .authenticationEntryPoint(authenticationEntryPointConfigurer));
        http.apply(filterConfigurerAdapter);

        return http.build();
    }

}
