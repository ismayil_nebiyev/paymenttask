package az.ingress.paymenttask.security;

import io.jsonwebtoken.Claims;
import jakarta.servlet.http.HttpServletRequest;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TokenAuthRequestFilter implements AuthService{
    private final JwtService jwtService;
    private static final String AUTHORIZATION = "Authorization";
    private static final String BEARER = "Bearer";

    @Override
    public Optional<Authentication> getAuthentication(HttpServletRequest request) {

        var authorization = request.getHeader(AUTHORIZATION);
        String token;
        if(authorization != null && authorization.startsWith(BEARER)){
            token = authorization.substring(BEARER.length());
            Claims claims = jwtService.parseToken(token);
            Authentication authenticationBearer = jwtService.getAuthenticationBearer(claims);
            return Optional.of(authenticationBearer);
        }
        else {
            return Optional.empty();
        }
    }
}
