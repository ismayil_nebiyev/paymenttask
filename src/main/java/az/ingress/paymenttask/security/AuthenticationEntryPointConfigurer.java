package az.ingress.paymenttask.security;

import static az.ingress.paymenttask.exception.PaymentErrorCodes.INVALID_CREDENTIALS;

import az.ingress.paymenttask.exception.ErrorResponseDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.time.OffsetDateTime;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

@Component()
public class AuthenticationEntryPointConfigurer implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException)
            throws IOException, ServletException {

        var path = request.getRequestURL().toString();

        ErrorResponseDTO re = ErrorResponseDTO.builder()
                .status(400)
                .code(INVALID_CREDENTIALS.code)
                .path(request.getRequestURI())
                .timestamp(OffsetDateTime.now())
                .message(INVALID_CREDENTIALS.code)
                .detail("Invalid credentials")
                .build();
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        OutputStream responseStream = response.getOutputStream();
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        mapper.registerModule(new JavaTimeModule());
        mapper.writeValue(responseStream, re);
        responseStream.flush();
    }
}