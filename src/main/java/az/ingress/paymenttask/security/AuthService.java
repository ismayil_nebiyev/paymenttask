package az.ingress.paymenttask.security;

import jakarta.servlet.http.HttpServletRequest;
import java.util.Optional;
import org.springframework.security.core.Authentication;

public interface AuthService {

    /**
     * Extract authentication object out of request.
     *
     * @param httpServletRequest : the http servlet request
     * @return : extracted Authentication
     */
    Optional<Authentication> getAuthentication(HttpServletRequest httpServletRequest);

}
