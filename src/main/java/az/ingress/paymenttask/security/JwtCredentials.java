package az.ingress.paymenttask.security;

import az.ingress.paymenttask.domain.enumeration.Role;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;

@FieldNameConstants
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class JwtCredentials {

    Long iat;
    String username;
    Long userId;
    List<Role> role;
}
