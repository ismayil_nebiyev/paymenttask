package az.ingress.paymenttask.service;

import com.api.first.development.openapi.model.PaymentDto;
import java.util.List;

public interface PaymentService {

    PaymentDto save(PaymentDto paymentDto);

    List<PaymentDto> findAll();

    PaymentDto search(String phone);
}
