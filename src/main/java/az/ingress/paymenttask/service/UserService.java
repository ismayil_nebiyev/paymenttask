package az.ingress.paymenttask.service;

import az.ingress.paymenttask.domain.Authority;
import az.ingress.paymenttask.domain.User;
import az.ingress.paymenttask.domain.enumeration.Role;
import az.ingress.paymenttask.exception.AuthorityNotFoundException;
import az.ingress.paymenttask.exception.PasswordDidNotMatchException;
import az.ingress.paymenttask.exception.UserAlreadyExistException;
import az.ingress.paymenttask.repository.AuthorityRepository;
import az.ingress.paymenttask.repository.UserRepository;
import az.ingress.paymenttask.security.JwtService;
import com.api.first.development.openapi.model.LoginRequestDto;
import com.api.first.development.openapi.model.LoginResponseDto;
import com.api.first.development.openapi.model.RegisterRequestDto;
import com.api.first.development.openapi.model.UserDto;
import java.util.Set;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserService {

    private final UserRepository userRepository;
    private final AuthorityRepository authorityRepository;
    private final JwtService jwtService;
    private final ModelMapper mapper;

    private final PasswordEncoder passwordEncoder;

    public void register(RegisterRequestDto registerRequestDto){
        if(userRepository.findByUsername(registerRequestDto.getUsername()).isPresent()){
            throw new UserAlreadyExistException(registerRequestDto.getUsername());
        }
        var user = mapper.map(registerRequestDto,User.class);
        var authority = authorityRepository.findByRole(Role.MANAGER)
                .orElseThrow(AuthorityNotFoundException::new);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setAuthorities(Set.of(authority));
        userRepository.save(user);
    }

    public LoginResponseDto login(RegisterRequestDto registerRequestDto){
        User user = userRepository.findByUsername(registerRequestDto.getUsername())
                .orElseThrow(() -> new UsernameNotFoundException(registerRequestDto.getUsername()));
        boolean matches = passwordEncoder.matches(registerRequestDto.getPassword(), user.getPassword());
        if(matches){
            return LoginResponseDto.builder()
                    .verificationToken(jwtService.issueToken(user))
                    .build();
        }
        else {
            throw new PasswordDidNotMatchException();
        }
    }

}
