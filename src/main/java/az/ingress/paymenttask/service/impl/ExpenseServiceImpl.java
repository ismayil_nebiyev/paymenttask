package az.ingress.paymenttask.service.impl;

import az.ingress.paymenttask.domain.Expense;
import az.ingress.paymenttask.exception.ExpenseNotFoundException;
import az.ingress.paymenttask.repository.ExpenseRepository;
import az.ingress.paymenttask.service.ExpenseService;
import com.api.first.development.openapi.model.ExpenseDto;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class ExpenseServiceImpl implements ExpenseService {

    private final ExpenseRepository expenseRepository;
    private final ModelMapper mapper;


    @Override
    public ExpenseDto save(ExpenseDto expenseDto) {
        if (expenseDto.getId() != null){
            Expense expense = findExpense(expenseDto.getId());
            expense.setCost(expenseDto.getCost());
            expense.setName(expenseDto.getName());
            expense.setDate(expenseDto.getDate());
            expense.setText(expenseDto.getText());
            expense.setImage(expenseDto.getImage());
            return mapper.map(expenseRepository.save(expense), ExpenseDto.class);
        }
        return mapper.map(expenseRepository.save(mapper.map(expenseDto,Expense.class)), ExpenseDto.class);
    }

    @Override
    public List<ExpenseDto> findAll() {
        return expenseRepository.findAll().stream()
                .map(expense -> mapper.map(expense, ExpenseDto.class)).collect(Collectors.toList());
    }

    private Expense findExpense(Long id){
        return expenseRepository.findById(id).orElseThrow(ExpenseNotFoundException::new);
    }
}
