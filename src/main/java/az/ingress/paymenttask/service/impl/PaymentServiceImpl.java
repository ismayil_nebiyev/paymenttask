package az.ingress.paymenttask.service.impl;

import az.ingress.paymenttask.domain.Course;
import az.ingress.paymenttask.domain.Payment;
import az.ingress.paymenttask.domain.Student;
import az.ingress.paymenttask.exception.PaymentNotFoundException;
import az.ingress.paymenttask.repository.PaymentRepository;
import az.ingress.paymenttask.service.PaymentService;
import com.api.first.development.openapi.model.CourseDto;
import com.api.first.development.openapi.model.PaymentDto;
import jakarta.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class PaymentServiceImpl implements PaymentService {

    private final PaymentRepository paymentRepository;
    private final ModelMapper mapper;

    @Override
    @Transactional
    public PaymentDto save(PaymentDto paymentDto) {
        if (paymentDto.getId() != null){
            Payment payment = findPayment(paymentDto.getId());
            payment.setAmount(paymentDto.getAmount());
            payment.setDate(paymentDto.getDate());
            payment.setMonth(paymentDto.getMonth());
            payment.setCourse(mapper.map(paymentDto.getCourse(),Course.class));
            payment.setStudent(mapper.map(paymentDto.getStudent(), Student.class));
            payment.setCheckImage(paymentDto.getCheckImage());
            payment.setCardholder(paymentDto.getCardholder());
            log.info(payment.toString());
            return mapper.map(paymentRepository.save(payment), PaymentDto.class);
        }
        return mapper.map(paymentRepository.save(mapper.map(paymentDto,Payment.class)), PaymentDto.class);
    }

    @Override
    public List<PaymentDto> findAll() {
        return paymentRepository.findAll().stream()
                .map(payment -> mapper.map(payment, PaymentDto.class)).collect(Collectors.toList());
    }

    @Override
    public PaymentDto search(String phone) {
        Payment payment = paymentRepository.findByStudent_Phone(phone).orElseThrow(PaymentNotFoundException::new);
        return mapper.map(payment, PaymentDto.class);
    }

    private Payment findPayment(Long id){
        return paymentRepository.findById(id).orElseThrow(PaymentNotFoundException::new);
    }
}
