package az.ingress.paymenttask.service.impl;

import java.util.Locale;
import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TranslationRepoServiceImpl {

    private final MessageSource messageSource;

    public String findByKey(String key,String lang,Object... arguments){
        try {
            return messageSource.getMessage(key,arguments,new Locale(lang));
        }catch (NoSuchMessageException ex){
            return "";
        }
    }
}
