package az.ingress.paymenttask.service.impl;

import az.ingress.paymenttask.domain.Student;
import az.ingress.paymenttask.exception.StudentNotFoundException;
import az.ingress.paymenttask.repository.StudentRepository;
import az.ingress.paymenttask.service.StudentService;
import com.api.first.development.openapi.model.StudentDto;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;
    private final ModelMapper mapper;

    @Override
    public void save(StudentDto dto) {
        if (dto.getId() != null){
            Student student = findStudent(dto.getId());
            student.setFirstname(dto.getFirstname());
            student.setSecondname(dto.getSecondname());
            student.setEmail(dto.getEmail());
            student.setPhone(dto.getPhone());
            studentRepository.save(student);

        }else {
            studentRepository.save(mapper.map(dto,Student.class));
        }
    }

    @Override
    public List<StudentDto> findAll() {
        return studentRepository.findAll().stream()
                .map(student -> mapper.map(student,StudentDto.class)).collect(Collectors.toList());
    }

    @Override
    public StudentDto findById(Long id) {
        Student student = findStudent(id);
        return mapper.map(student, StudentDto.class);
    }

    @Override
    public StudentDto findByPhone(String phone) {
        Student student = studentRepository.findStudentByPhone(phone).orElseThrow(StudentNotFoundException::new);
        return mapper.map(student, StudentDto.class);
    }

    @Override
    public void delete(Long id) {
        studentRepository.deleteById(id);
    }

    private Student findStudent(Long id){
        return studentRepository.findById(id).orElseThrow(StudentNotFoundException::new);
    }
}
