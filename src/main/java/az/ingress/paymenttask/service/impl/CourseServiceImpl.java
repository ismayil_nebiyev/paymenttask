package az.ingress.paymenttask.service.impl;

import az.ingress.paymenttask.domain.Course;
import az.ingress.paymenttask.domain.Student;
import az.ingress.paymenttask.exception.CourseNotFoundException;
import az.ingress.paymenttask.repository.CourseRepository;
import az.ingress.paymenttask.repository.StudentRepository;
import az.ingress.paymenttask.service.CourseService;
import com.api.first.development.openapi.model.CourseDto;
import com.api.first.development.openapi.model.JoinRequestDto;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class CourseServiceImpl implements CourseService {

    private final CourseRepository courseRepository;
    private final StudentRepository studentRepository;
    private final ModelMapper mapper;
    @Override
    public CourseDto save(CourseDto courseDto) {
        if(courseDto.getId() != null){
            Course course = findCourse(courseDto.getId());
            course.setName(courseDto.getName());
            course.setActive(courseDto.getActive());
            List<Student> students = courseDto.getMembers()
                    .stream().map(student -> mapper.map(student,Student.class)).collect(Collectors.toList());
            course.setMembers(students);
            return mapper.map(courseRepository.save(course),CourseDto.class);
        }
        return mapper.map(courseRepository.save(mapper.map(courseDto,Course.class)), CourseDto.class);
    }

    @Override
    public List<CourseDto> getCourseList() {
        return courseRepository.findAll().stream()
                .map(course -> mapper.map(course, CourseDto.class)).collect(Collectors.toList());
    }

    @Override
    public CourseDto getCourse(String name) {
        Course course = findCourseByName(name);
        return mapper.map(course, CourseDto.class);
    }

    @Override
    public CourseDto addStudent(JoinRequestDto joinRequestDto) {
        Course course = findCourseByName(joinRequestDto.getCourseName());
        Student student = findStudent(joinRequestDto.getStudentPhone());
        course.setMembers(List.of(student));
        return mapper.map(courseRepository.save(mapper.map(course,Course.class)), CourseDto.class);
    }

    private Course findCourse(Long id){
        return courseRepository.findById(id).orElseThrow(CourseNotFoundException::new);
    }
    private Course findCourseByName(String name){
        return courseRepository.findCourseByName(name).orElseThrow(CourseNotFoundException::new);
    }

    private Student findStudent(String phone){
        return studentRepository.findStudentByPhone(phone).orElseThrow(CourseNotFoundException::new);
    }
}
