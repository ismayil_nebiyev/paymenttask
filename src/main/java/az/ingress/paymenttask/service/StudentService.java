package az.ingress.paymenttask.service;

import com.api.first.development.openapi.model.StudentDto;
import java.util.List;
import java.util.Optional;

public interface StudentService {

    void save(StudentDto dto);

    List<StudentDto> findAll();

    StudentDto findById(Long id);

    StudentDto findByPhone(String phone);

    void delete(Long id);
}
