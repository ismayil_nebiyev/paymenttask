package az.ingress.paymenttask.service;

import com.api.first.development.openapi.model.ExpenseDto;
import com.api.first.development.openapi.model.PaymentDto;
import java.util.List;

public interface ExpenseService {

    ExpenseDto save(ExpenseDto expenseDto);

    List<ExpenseDto> findAll();
}
