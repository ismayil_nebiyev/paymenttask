package az.ingress.paymenttask.service;

import az.ingress.paymenttask.domain.Course;
import com.api.first.development.openapi.model.CourseDto;
import com.api.first.development.openapi.model.JoinRequestDto;
import java.util.List;

public interface CourseService {

    CourseDto save(CourseDto courseDto);

    List<CourseDto> getCourseList();

    CourseDto getCourse(String name);

    CourseDto addStudent(JoinRequestDto joinRequestDto);
}
