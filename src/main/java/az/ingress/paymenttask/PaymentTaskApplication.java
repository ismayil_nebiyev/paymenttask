package az.ingress.paymenttask;

import az.ingress.paymenttask.domain.Authority;
import az.ingress.paymenttask.domain.User;
import az.ingress.paymenttask.domain.enumeration.Role;
import az.ingress.paymenttask.repository.AuthorityRepository;
import az.ingress.paymenttask.repository.UserRepository;
import java.util.Set;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
@Slf4j
public class PaymentTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(PaymentTaskApplication.class, args);
	}

}
