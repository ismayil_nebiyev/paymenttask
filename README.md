# PAYMENT-APP

## Tech Stack
- Gradle
- Java 17
- Spring BOOT (Web, Security, Data JPA)
- JPA Specification
- MySQL 
- ModelMapper
- Docker


## Installation and Running
##### After Cloning project, Open your favorite Terminal in root directory and run this command.


    docker-compose up
    
## Postman Collections to import [click here](https://gitlab.com/ismayil_nebiyev/paymenttask/-/blob/main/Payment%20Task.postman_collection.json)
#### By importing `Payment Task.postman_collection.json` file into Postman (which located in root directory),  You can quickly test an application 

##### default base url: 
      
      localhost:8002
     
## REST API
##### When Application running, firstly by default Admin user will be created AS ADMIN Role

### Features

- The user cannot register himself, only the administrator can register a new user and give him/her an account (by default, the user has the manager role)
- The manager can add courses and their students, as well as edit them.
- The manager can also add payments, such as information about how much money a student paid for which course, an image of the check, etc.
- In addition, if there are any expenses, the manager can also record them.

### Authentication
##### Get access_token to send other requests
`POST {{base_url}}/auth/v1/login`

``` 
{
    "username":"admin",
    "password":"password"
}
```      

### Student APIs

| Request                     | About                                 |
|-----------------------------|---------------------------------------|
| `PUT /v1/students`          | Create or update an existing student. |
| `GET /v1/students/{id}`     | Finds student by id.                  |
| `GET /v1/students/search`   | Finding a student by phone            |
| `DELETE /v1/students/{id}`  | Delete student by id.                 |
| `GET /v1/students/list`     | Get student list.                     |

### Course APIs

| Request                     | About                                |
|-----------------------------|--------------------------------------|
| `PUT /v1/courses`           | Create or update an existing course. |
| `GET /v1/courses/search`    | Finding a course by name             |
| `POST /v1/courses/students` | Add a new student to the course.     |
| `GET /v1/courses/list`      | Get course list.                     |

### Payment APIs

| Request                     | About                                |
|-----------------------------|--------------------------------------|
| `PUT /v1/payments`           | Create or update an existing payment.|
| `GET /v1/payments/search`    | Finding a payment by student phone   |
| `GET /v1/payments/list`      | Get payment list.                    |

### Expense APIs

| Request                     | About                                 |
|-----------------------------|---------------------------------------|
| `PUT /v1/expenses`          | Create or update an existing expense. |
| `GET /v1/expenses/list`     | Get expense list.                     |




    

